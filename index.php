﻿<?php

$servername = 'localhost';
$username = 'root';
$password = 'MySQL_123';
$database = 'colyseum';

$conn = new mysqli($servername, $username, $password, $database);
if ($conn->connect_error) {
    die("Connexion échouée : $conn->connect_error<br>");
}

// Exercice 1

$sql_clients = $conn->query("SELECT * FROM clients");

$clients = $sql_clients->fetch_all(MYSQLI_ASSOC);

echo "Clients :<br><br>";

foreach ($clients as $client) {
    echo $client['firstName'].' '.$client['lastName'].'<br>';
}

echo "<br><br>";

// Exercice 2

$sql_showTypes = $conn->query("SELECT * FROM `showTypes`");

$showTypes = $sql_showTypes->fetch_all(MYSQLI_ASSOC);

echo "Types de spectacles :<br><br>";

foreach ($showTypes as $showType) {
    echo $showType['type'].'<br>';
}

echo "<br><br>";

// Exercice 3

$sql_twentyFirst = $conn->query("SELECT * FROM clients WHERE id <= 20");

$twentyFirst = $sql_twentyFirst->fetch_all(MYSQLI_ASSOC);

echo "20 premiers clients :<br><br>";

foreach ($twentyFirst as $client) {
    echo $client['id'].' '.$client['firstName'].' '.$client['lastName'].'<br>';
}

echo "<br><br>";

// Exercice 4

$sql_hasCard = $conn->query("SELECT * FROM clients WHERE card");

$hasCard = $sql_hasCard->fetch_all(MYSQLI_ASSOC);

echo "Clients qui ont une carte :<br><br>";

foreach ($hasCard as $client) {
    echo $client['firstName'].' '.$client['lastName'].' (Numéro de carte : '.$client['cardNumber'].')'.'<br>';
}

echo "<br><br>";

// Exercice 5

$sql_startM = $conn->query("SELECT * FROM clients WHERE lastName LIKE 'M%' ORDER BY lastName ASC");

$startM = $sql_startM->fetch_all(MYSQLI_ASSOC);

echo "Clients dont le nom commence par M :<br><br>";

foreach ($startM as $client) {
    echo 'Nom : '.$client['lastName'].'<br>';
    echo 'Prénom : '.$client['firstName'].'<br>';
    echo '<br>';
}

echo "<br><br>";

// Exercice 6

$sql_shows = $conn->query("SELECT * FROM shows ORDER BY title ASC");

$shows = $sql_shows->fetch_all(MYSQLI_ASSOC);

echo "Spectacles :<br><br>";

foreach ($shows as $show) {
    echo $show['title'].' par '.$show['performer'].' le '.$show['date'].' à '.$show['startTime'].'<br>';
}

echo "<br><br>";

// Exercice 7

$sql_detailed_clients = $conn->query("SELECT * FROM clients");

$detailed_clients = $sql_detailed_clients->fetch_all(MYSQLI_ASSOC);

echo "Clients (détails) :<br><br>";

foreach ($detailed_clients as $client) {
    echo 'Nom : '.$client['lastName'].'<br>';
    echo 'Prénom : '.$client['firstName'].'<br>';
    echo 'Date de naissance : '.$client['birthDate'].'<br>';
    if ($client['card']) {
        echo 'Carte de fidélité : Oui<br>';
        echo 'Numéro de carte : '.$client['cardNumber'].'<br>';
    } else {
        echo 'Carte de fidélité : Non<br>';
    }
    echo '<br>';
}

echo "<br><br>";

?>
